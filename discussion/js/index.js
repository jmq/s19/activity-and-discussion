// Exponent Operator
const firstNum = Math.pow(8, 2)
console.log(firstNum)

// ES6 Update
// added exponent operator5**3
// `` - template literal
// [] - array literal
// {} - object literals
// ${} - placeholder


const secondNum = 8 ** 2
console.log(secondNum)

// Array Destructoring
// - Allows us t unpack in arrays into distinct variablse.
// - It allows us to name array elements with variables instead of using the index number

